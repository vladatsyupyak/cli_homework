import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products.component';
import {InputModule} from "../../shared/input/input.module";
import {SharedModule} from "../../shared/shared.module";
import {ButtonModule} from "../../shared/button/button.module";



@NgModule({
  declarations: [
    ProductComponent,
    ProductsComponent,

  ],
  exports: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    InputModule,
    SharedModule,
    ButtonModule
  ]
})
export class ProductsModule { }
