import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import {SharedModule} from "../../shared/shared.module";
import {ButtonModule} from "../../shared/button/button.module";



@NgModule({
  declarations: [
    CartComponent
  ],
  exports: [
    CartComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ButtonModule
  ]
})
export class CartModule { }
